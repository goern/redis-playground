FROM registry.access.redhat.com/rhel7-atomic

MAINTAINER <goern@b4mad.net>

LABEL io.k8s.description="a tiny redis tester" \
      io.openshift.tags="healthz,redis,test"

COPY redis-playground /

CMD ["/redis-playground"]
