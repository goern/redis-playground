package main

import (
	"fmt"
	"os"
	"time"

	"github.com/go-redis/redis"
)

func main() {
	fmt.Printf("This is just a small loop...\nPinging redis now\n")

	// FIXME(goern) lets use the sentinel ....
	client := redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDIS_SERVICE_HOST") + ":6379",
		Password: os.Getenv("REDIS_PASSWORD"),
		DB:       0, // use default DB
	})

	stillPinging := 0

	for stillPinging < 100 {
		pong, err := client.Ping().Result()

		if err != nil {
			fmt.Println(err)
			stillPinging = 255
		} else {
			fmt.Println(pong)
		}

		time.Sleep(2 * time.Second)
		stillPinging++
	}

	os.Exit(0)
}
