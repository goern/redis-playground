all:
	go build
	docker build --rm --tag goern-docker-registry.bintray.io/testing/redis-playground:latest .
	docker push goern-docker-registry.bintray.io/testing/redis-playground:latest

deploy:
	oc delete pod redis-playground
	oc create -f redis-playground-pod.yaml

clean:
	rm redis-playground
